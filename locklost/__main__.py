import os
import subprocess
import argparse
import logging

from . import __version__, set_signal_handlers
from . import config
from . import search
from . import analyze
from . import online
from . import event
from . import segments
from . import summary
from . import plots

##########

parser = argparse.ArgumentParser(
    prog='locklost',
)
subparser = parser.add_subparsers(
    title='Commands',
    metavar='<command>',
    dest='cmd',
    #help=argparse.SUPPRESS,
)
subparser.required = True

def gen_subparser(cmd, func):
    assert func.__doc__, "empty docstring: {}".format(func)
    help = func.__doc__.split('\n')[0].lower().strip('.')
    desc = func.__doc__.strip()
    p = subparser.add_parser(
        cmd,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help=help,
        description=desc,
    )
    p.set_defaults(func=func)
    return p

##########

parser.add_argument(
    '--version', action='version', version='%(prog)s {}'.format(__version__),
    help="print version and exit")


p = gen_subparser('search', search.main)
search._parser_add_arguments(p)


p = gen_subparser('analyze', analyze.main)
analyze._parser_add_arguments(p)


p = gen_subparser('online', online.main)
online._parser_add_arguments(p)


p = gen_subparser('summary', summary.main)
summary._parser_add_arguments(p)


def list_events(args):
    """List all events"""
    for e in event.find_events():
        if e.analyzed:
            aflag = 'A'
        else:
            aflag = '-'
        print('{} {}->{} {} {}'.format(
            e.id,
            e.transition_index[0],
            e.transition_index[1],
            aflag,
            e.path(),
        ))

p = gen_subparser('list', list_events)


def show_event(args):
    """Show event info

    """
    e = event.LocklossEvent(args.event)
    if args.log:
        subprocess.call(['less', '+G', e.path('log')])
        exit()
    print("id: {}".format(e.id))
    print("gps: {}".format(e.gps))
    print("path: {}".format(e.path()))
    print("url: {}".format(e.url()))
    print("transition: {}".format(e.transition_index))
    print("analysis: {}".format(e.analysis_version))
    print("status: {}".format(e.analysis_status))
    print("tags: {}".format(' ' .join(e.list_tags())))
    # print("files:")
    # subprocess.call(['find', e.path()])

p = gen_subparser('show', show_event)
p.add_argument('--log', '-l', action='store_true',
               help="show event log")
p.add_argument('event',
               help="event ID")


def tag_events(args):
    """Tag events

    If no tag operations are specified, the tags for the specified
    event will be printed.

    """
    e = event.LocklossEvent(args.event)
    if not args.tags:
        for tag in e.list_tags():
            print(tag)
        return
    for tag in args.tags:
        if tag[0] == '+':
            e.add_tag(tag[1:])
        elif tag[0] == '-':
            e.rm_tag(tag[1:])
        else:
            e.add_tag(tag)

p = gen_subparser('tag', tag_events)
p.add_argument('event',
               help="event ID")
p.add_argument('tags', metavar='+/-TAG', nargs='*',
               help="tag operation")


def compress_segments(args):
    """Compress completed segment cache

    """
    segments.compress_segdir(config.SEG_DIR)

p = gen_subparser('compress', compress_segments)


def mask_segment(args):
    """Mask bad segment

    Marks segment as analyzed.

    FIXME: should be tracked separately

    """
    from gwpy.segments import Segment
    segments.write_segments(
        config.SEG_DIR,
        [Segment(args.start, args.end)],
    )

p = gen_subparser('mask', mask_segment)
p.add_argument('start', type=int,
               help="segment start")
p.add_argument('end', type=int,
               help="segment end")


def plot_history(args):
    """Plot history of events

    """
    plots.plot_history(args.path, lookback=args.lookback)

p = gen_subparser('plot-history', plot_history)
p.add_argument('path',
               help="output file")
p.add_argument('-l', '--lookback', default='7 days ago',
               help="history look back ['7 days ago']")

##########

def main():
    set_signal_handlers()
    logging.basicConfig(
        level=os.getenv('LOG_LEVEL', 'INFO'),
        format=config.LOG_FMT,
    )
    args = parser.parse_args()
    if not config.EVENT_ROOT:
        raise SystemExit("Must specify LOCKLOST_EVENT_ROOT env var.")
    args.func(args)


if __name__ == '__main__':
    main()
