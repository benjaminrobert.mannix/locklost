import os
import shutil
import logging
import numpy as np

from gpstime import tconvert

from . import config

##################################################

def _trans_int(trans):
    return tuple(int(i) for i in trans)


class LocklossEvent(object):
    __slots__ = [
        '__id', '__epath',
        '_transition_index', '__transition_gps',
        '__refined_gps', '__previous_state',
    ]

    def __init__(self, gps):
        """Retrieve existing event

        Throws an OSError if an event for the specified time does not
        exist.

        """
        try:
            self.__id = int(gps)
        except ValueError:
            self.__id = int(gps.replace('/', ''))
        self.__epath = self._gen_epath(self.__id)
        if not os.path.exists(self.path()):
            raise OSError("Unknown event: {}".format(self.path()))
        self._transition_index = None
        self.__transition_gps = None
        self.__refined_gps = None
        self.__previous_state = None

    @staticmethod
    def _gen_epath(gps):
        gpss = str(int(gps))
        # we split event paths into 1000000 second / 11.6 day epochs
        epoch, esecs = gpss[:-6], gpss[-6:]
        return os.path.join(epoch, esecs)

    @property
    def id(self):
        """event ID"""
        return self.__id

    @property
    def epath(self):
        """event 'epoch/esecs' string"""
        return self.__epath

    @property
    def gps(self):
        """best estimate of event GPS time

        If the event has been refined this will return the refined
        GPS.  If not, the guardian transition time will be returned.

        """
        refined = self.refined_gps
        if refined and not np.isnan(refined):
            return refined
        return self.transition_gps

    @property
    def utc(self):
        """event UTC time"""
        return tconvert(self.gps)

    def path(self, *args):
        """event path, or path to event file"""
        return os.path.join(config.EVENT_ROOT, self.epath, *args)

    def url(self, *args):
        """event URL, or URL to event file"""
        return os.path.join(config.WEB_ROOT, 'events', self.epath, *args)

    @property
    def view_url(self):
        """event URL"""
        return '{}?event={}'.format(config.WEB_ROOT, self.id)

    @property
    def transition_index(self):
        """tuple of indices of Guardian state transition"""
        if not self._transition_index:
            with open(self.path('transition_index')) as f:
                tis = f.read().split()
            self._transition_index = _trans_int(tis)
        return self._transition_index

    @property
    def transition_gps(self):
        """GPS time of Guardian state transition"""
        if not self.__transition_gps:
            try:
                with open(self.path('guard_state_end_gps')) as f:
                    self.__transition_gps = float(f.read().strip())
            except:
                # if this is an old event and we don't have a
                # guard_state_end_gps file, just return the id (int of
                # transition time) as the transition GPS
                return float(self.id)
        return self.__transition_gps

    def __str__(self):
        return str(self.id)

    def __repr__(self):
        return '<LocklossEvent: {}, {}->{}>'.format(
            self.id,
            *self.transition_index)

    #####

    @classmethod
    def create(cls, gps, transition_index=None):
        """create new event directory

        An OSError will be thrown if the event already exists.

        """
        path = os.path.join(config.EVENT_ROOT, cls._gen_epath(gps))
        try:
            os.makedirs(os.path.dirname(path))
        except OSError:
            pass
        try:
            os.makedirs(path)
        except OSError:
            raise OSError("Event {} already exists.".format(path))
        event = cls(gps)
        with open(event.path('guard_state_end_gps'), 'w') as f:
            f.write('{:f}\n'.format(gps))
        if transition_index:
            with open(event.path('transition_index'), 'w') as f:
                f.write('{:.0f} {:.0f}\n'.format(*transition_index))
        logging.info("event created: {}".format(event.path()))
        return event

    def _scrub(self, archive=True):
        """scrub event analysis artifacts

        If archive=True the artificats will be archived in a
        "bak/<mtime>" directory.

        """
        preserve_files = [
            'guard_state_end_gps',
            'transition_index',
            'log',
            'lock',
            'condor',
            'bak',
        ]
        if archive:
            try:
                last_time = int(os.stat(self.path('status')).st_mtime)
            except OSError:
                return
            bak_dir = self.path('bak', str(last_time))
            logging.info("archiving old artifacts: {}".format(bak_dir))
            try:
                os.makedirs(bak_dir)
            except OSError:
                pass
            for f in os.listdir(self.path()):
                if f in preserve_files:
                    continue
                shutil.move(self.path(f), bak_dir)
        else:
            logging.info("purging old artifacts...")
            for f in os.listdir(self.path()):
                if f in preserve_files:
                    continue
                try:
                    shutil.rmtree(self.path(f))
                except OSError:
                    os.remove(self.path(f))

    #####

    def _set_version(self, version):
        path = self.path('version')
        with open(path, 'w') as f:
            f.write(version+'\n')

    def _set_status(self, status):
        assert status in [None, 0, 1]
        path = self.path('status')
        if status is None:
            try:
                os.remove(path)
            except OSError:
                pass
        else:
            with open(path, 'w') as f:
                f.write(str(status)+'\n')

    def lock(self):
        """lock event for analysis

        Throws an OSError if the event is already locked.

        """
        path = self.path('lock')
        if os.path.exists(path):
            raise OSError("Event already being analyzed: {}".format(path))
        open(path, 'w').close()

    def __enter__(self):
        """Enter context manager"""
        self.lock()
        return self

    def release(self, *args, **kwargs):
        """release event analysis lock"""
        try:
            os.remove(self.path('lock'))
        except OSError:
            pass

    def __exit__(self, exception_type, exception_value, traceback):
        """Exit context manager"""
        self.release()

    @property
    def analyzing(self):
        """True if event is being analyzed"""
        return os.path.exists(self.path('lock'))

    @property
    def analyzed(self):
        """True if event has been analyzed"""
        return os.path.exists(self.path('status')) or os.path.exists(self.path('analyzed'))

    @property
    def analysis_status(self):
        """analysis status

        0=success, 1=fail

        """
        path = self.path('status')
        if not os.path.exists(path):
            return None
        with open(path, 'r') as f:
            return int(f.readline().strip())

    @property
    def analysis_succeeded(self):
        """True if analysis succeeded"""
        return self.analysis_status == 0

    @property
    def analysis_version(self):
        """analysis version

        or None if not analyzed

        """
        path = self.path('version')
        if not os.path.exists(path):
            path = self.path('analyzed')
        if not os.path.exists(path):
            return
        with open(path, 'r') as f:
            return f.readline().strip()

    #####

    @property
    def _tag_dir(self):
        return self.path('tags')

    def _tag_path(self, tag):
        return os.path.join(self._tag_dir, tag.upper())

    def list_tags(self):
        """list of event tags"""
        try:
            return os.listdir(self._tag_dir)
        except OSError:
            return []

    def has_tag(self, tag):
        """True if event has tag"""
        return os.path.exists(self._tag_path(tag))

    def add_tag(self, *tags):
        """add tags

        Tags should be simple strings without spaces.

        """
        try:
            os.mkdir(self._tag_dir)
        except OSError:
            pass
        for tag in tags:
            assert ' ' not in tag, "Spaces are not permitted in tags."
            open(self._tag_path(tag), 'a').close()
            logging.info("added tag: {}".format(tag))

    def rm_tag(self, *tags):
        """remove tags"""
        for tag in tags:
            try:
                os.remove(self._tag_path(tag))
            except OSError:
                pass

    #####

    @property
    def refined(self):
        """True if event has had time refined"""
        return self.has_tag('REFINED')

    @property
    def refined_gps(self):
        """refined gps time of event

        Or None if the event time was not refined.

        """
        if not self.__refined_gps:
            path = self.path('refined_gps')
            if not os.path.exists(path):
                return None
            with open(path) as f:
                gps = f.read().strip()
            if gps in [None, 'nan', float('nan')]:
                self.__refined_gps = float('nan')
            else:
                self.__refined_gps = float(gps)
        if np.isnan(self.__refined_gps):
            return None
        return self.__refined_gps

    @property
    def previous_state(self):
        """previous guardian state

        Returns dict of (index, start, end)

        """
        if not self.__previous_state:
            path = self.path('previous_state')
            if not os.path.exists(path):
                return None
            with open(path) as f:
                s = f.read().strip().split()
                self.__previous_state = {
                    'index': int(s[0]),
                    'start': float(s[1]),
                    'end': float(s[2]),
                }
        return self.__previous_state

    def to_dict(self):
        return {
            'id': self.id,
            'gps': self.gps,
            'utc': self.utc,
            'url': self.url(),
            'view_url': self.view_url,
            'transition_index': self.transition_index,
            'analyzed': self.analyzed,
            'analysis_version': self.analysis_version,
            'refined': self.refined,
            'refined_gps': self.refined_gps,
            'tags': self.list_tags(),
            'previous_state': self.previous_state,
        }

##################################################

def _generate_all_events():
    """generate all events in reverse chronological order"""
    event_root = config.EVENT_ROOT
    for epoch in sorted(os.listdir(event_root), reverse=True):
        try:
            int(epoch)
        except ValueError:
            continue
        for esec in sorted(os.listdir(os.path.join(event_root, epoch)), reverse=True):
            id = epoch + esec
            try:
                yield LocklossEvent(id)
            except ValueError:
                continue


def find_events(**kwargs):
    """generator of events matching search term

    Possible terms are:

      after: event after this GPS time
      before: event before this GPS time
      state: previous state equals this state
      tag: event tag present

    Events are generated in reverse chronological order.

    """
    states = set()
    if 'state' in kwargs:
        comma_sep = [s.strip() for s in kwargs['state'].split(',')]
        for item in comma_sep:
            ss = [int(s) for s in item.split('-')]
            if len(ss) > 1:
                ss = range(ss[0], ss[-1]+1)
            states.update(ss)

    for event in _generate_all_events():
        trans_idx = event.transition_index[0]

        if 'after' in kwargs and event.gps < kwargs['after']:
            # events generated in reverse chronological order, so we
            # can just break at the first event before the requested
            # search range
            break

        if 'before' in kwargs and event.gps > kwargs['before']:
            continue

        if states and trans_idx not in states:
            continue

        if 'tag' in kwargs:
            if not set(kwargs['tag']) <= set(event.list_tags()):
                continue

        yield event
