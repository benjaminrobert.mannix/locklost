import os
from datetime import datetime

import bottle

from .. import config
from ..event import LocklossEvent
from . import utils

##################################################

template_path = os.path.join(os.path.dirname(__file__), 'templates')
bottle.TEMPLATE_PATH.insert(0, template_path)

# NOTE: shouldn't be needed, but url paths include this from apache
WEB_SCRIPT = config.WEB_ROOT + '/index.cgi'

##################################################

def query_parse(query):
    """parse query and return dictionary

    Also inserts a limit of QUERY_DEFAULT_LIMIT if none is specified
    and no date range is specified.

    """
    val_conv = {
        'after': int,
        'before': int,
        'state': str,
        'tag': list,
        'limit': int,
    }

    # limit events shown if date range hasn't been specified
    if 'limit' not in query and \
       not ('after' in query and 'before' in query):
        query['limit'] = config.QUERY_DEFAULT_LIMIT

    out_query = {}
    for key, val in query.items():
        # if the query key is not known ignore
        if key not in val_conv:
            continue
        # if the value is empty skip
        if not val:
            continue
        if key == 'tag':
            val = [tag for tag in bottle.request.query.getall('tag') \
                if isinstance(tag, str)]
        try:
            out_query[key] = val_conv[key](val)
        except ValueError:
            # FIXME: raise better error here?
            raise

    return out_query


def online_status():
    """HTML-formatted online status information

    """
    stat_file = os.path.join(config.CONDOR_ONLINE_DIR, 'stat')
    if not os.path.exists(stat_file):
        return '<span style="color:red">ONLINE ANALYSIS NOT RUNNING</span>'
    stat = os.stat(stat_file)
    dt = datetime.fromtimestamp(stat.st_mtime)
    dtnow = datetime.now()
    age = dtnow - dt
    tsecs = age.total_seconds()
    if tsecs > 60:
        color = 'red'
    else:
        color = 'green'
    wcroot = os.path.join(config.WEB_ROOT, 'events', '.condor_online')
    return '<span style="color: {}">online last update: {:0.1f} min ago ({}) [<a href="{}">log</a>]</span>'.format(
        color,
        tsecs/60,
        dt,
        os.path.join(wcroot, 'out'),
    )

##################################################

app = bottle.Bottle()

@app.route("/")
@app.route("/tag/<tag>")
def index(tag='all'):

    if bottle.request.query.format == 'json':
        if 'event' in bottle.request.query:
            gps = bottle.request.query.get('event')
            try:
                event = LocklossEvent(gps)
            except (ValueError, OSError) as e:
                bottle.abort(404, {'error': {'code': 404, 'message': 'Unknown event: {}'.format(gps)}})
            return event.to_dict()

        query = query_parse(bottle.request.query)
        events = [event.to_dict() for event in utils.event_gen(query)]
        return {
            'events': events,
            'query': query,
        }

    elif 'event' in bottle.request.query:
        gps = bottle.request.query.get('event')
        try:
            event = LocklossEvent(gps)
        except (ValueError, OSError) as e:
            bottle.abort(404, 'Unknown event: {}'.format(gps))

        sat_channels = []
        csv_path = event.path('saturations.csv')
        if os.path.exists(csv_path):
            with open(csv_path, 'r') as f:
                for line in f:
                    sat_channels.append(line.split())

        return bottle.template(
            'event.tpl',
            IFO=config.IFO,
            web_script=WEB_SCRIPT,
            online_status=online_status(),
            is_home=False,
            event=event,
            tags=utils.tag_buttons(event.list_tags()),
            sat_channels=sat_channels,
        )

    else:
        is_home = not bool(bottle.request.query_string)
        query = query_parse(bottle.request.query)

        return bottle.template(
            'index.tpl',
            IFO=config.IFO,
            web_script=WEB_SCRIPT,
            online_status=online_status(),
            is_home=is_home,
            query=query,
        )

@app.route("/summary")
@app.route("/summary/<epoch>")
def summary_route(epoch=None):
    if not epoch:
        return
    return bottle.template(
        'summary.tpl',
        IFO=config.IFO,
        web_script=WEB_SCRIPT,
        online_status=online_status(),
        is_home=False,
        epoch=epoch,
    )

@app.route("/event/<gps>")
def event_route(gps):
    bottle.redirect('{}?event={}'.format(WEB_SCRIPT, gps))


@app.route("/tag/<tag>")
def tag_route(tag='all'):
    bottle.redirect('{}?tag={}&{}'.format(
        WEB_SCRIPT,
        tag,
        bottle.request.query_string,
    ))


@app.route("/api")
@app.route("/api/tag/<tag>")
def json_route(tag='all'):
    bottle.redirect('{}?format=json&{}'.format(
        WEB_SCRIPT,
        bottle.request.query_string,
    ))

##################################################

if __name__ == '__main__':
    bottle.run(app, server='cgi', debug=True)
