import os
import argparse
import logging

from . import set_signal_handlers
from . import config
from . import search
from . import analyze
from . import condor

##################################################

def start_job():
    try:
        os.makedirs(config.CONDOR_ONLINE_DIR)
    except OSError:
        pass
    sub = condor.CondorSubmit(
        config.CONDOR_ONLINE_DIR,
        'online',
        local=True,
        restart=True,
        notify_user=os.getenv('CONDOR_NOTIFY_USER'),
    )
    sub.write()
    sub.submit()
    print("""
Run the following to monitor:
watch condor_q -nobatch
tail -F {0}/{{log,out}}
""".format(config.CONDOR_ONLINE_DIR).strip())


def _parser_add_arguments(parser):
    parser = parser.add_subparsers(
        title='Commands',
        metavar='<command>',
        dest='cmd',
        #help=argparse.SUPPRESS,
    )
    parser.required = True
    ep = parser.add_parser(
        'exec',
        help="direct execute online analysis",
    )
    parser.add_parser(
        'start',
        help="start condor online analysis",
    )
    parser.add_parser(
        'stop',
        help="stop condor online analysis",
    )
    parser.add_parser(
        'restart',
        help="restart condor online analysis",
    )
    parser.add_parser(
        'status',
        help="print condor job status",
    )
    return parser


def main(args=None):
    """Online search for lockloss events

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    if args.cmd == 'exec':
        search.search_iterate(
            event_callback=analyze.analyze_condor,
        )

    elif args.cmd == 'start':
        ojobs, ajobs = condor.find_jobs()
        assert not ojobs, "There are already currently acitve jobs:\n{}".format(
            '\n'.join([condor.job_str(job) for job in ojobs]))
        start_job()

    elif args.cmd in ['stop', 'restart']:
        ojobs, ajobs = condor.find_jobs()
        condor.stop_jobs(ojobs)
        if args.cmd == 'restart':
            start_job()

    elif args.cmd == 'status':
        ojobs, ajobs = condor.find_jobs()
        for job in ojobs + ajobs:
            print(condor.job_str(job))


# direct execution of this module intended for condor jobs
if __name__ == '__main__':
    set_signal_handlers()
    logging.basicConfig(
        level='DEBUG',
        format='%(asctime)s %(message)s'
    )
    stat_file = os.path.join(config.CONDOR_ONLINE_DIR, 'stat')
    if not os.path.exists(stat_file):
        open(stat_file, 'w').close()
    search.search_iterate(
        event_callback=analyze.analyze_condor,
        stat_file=stat_file,
    )
